FROM openjdk:8u171-jdk
MAINTAINER Tuna Ozkasap <tuna.ozkasap@gmail.com>

# add our user and group first to make sure their IDs get assigned consistently, regardless of whatever dependencies get added
RUN useradd -m archiva
RUN usermod -G archiva archiva


ENV VERSION 2.2.3

#
# Adjust ownership and Perform the data directory initialization
#
ADD apache-archiva-2.2.3-bin.tar.gz /home/archiva/
RUN chmod +x /home/archiva/apache-archiva-2.2.3/bin/archiva
RUN chown -R archiva /home/archiva
#
# Add the bootstrap cmd
#
ADD run.bash /home/archiva/
RUN chmod +x /home/archiva/run.bash
RUN ls -al /bin/ && echo $PATH

VOLUME ["/home/archiva/apache-archiva-2.2.3/logs", "/home/archiva/apache-archiva-2.2.3/repositories"]

# Standard web ports exposted
EXPOSE 8080/tcp 8443/tcp

#What will be executed when container starts
CMD ["./home/archiva/apache-archiva-2.2.3/bin/archiva", "console"]


